from django.http import JsonResponse
from .models import Presentation
from .encoders import (
    PresentationListEncoder,
    PresentationDetailEncoder
)


def api_list_presentations(request, conference_id):
    presentations = Presentation.objects.all()
    return JsonResponse(
        {"presentations": presentations},
        encoder=PresentationListEncoder,
    )


def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
